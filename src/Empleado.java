import java.util.concurrent.ThreadLocalRandom;

public class Empleado {
    private final int ID = ThreadLocalRandom.current().nextInt(10000,50001);
    public int age;
    public String name;
    private String address;
    private int salary = ThreadLocalRandom.current().nextInt(10000,80000);
    private int bono = (salary>50000)?10000:5000;

    public Empleado(){}

    public Empleado(int age, String name, String address) {
        this.age = age;
        this.name = name;
        this.address = address;
    }

    public int getID() {
        return ID;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getBono() {
        return bono;
    }

    public void setBono(int bono) {
        this.bono = bono;
    }
}
