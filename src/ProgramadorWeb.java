import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class ProgramadorWeb extends Programador implements IActividades{
    public List<String> tecnologias_web;
    public Integer tiempo_experiencia = ThreadLocalRandom.current().nextInt(1,15);;

    public ProgramadorWeb(){

    }

    public void imprimeDatos(){
        System.out.println("Tecnologias Web");
        if(tecnologias_web!=null){
            for(String tw : tecnologias_web){
                System.out.print(tw + " ");
            }
        }
    }

    @Override
    public void actividad_actual() {
        System.out.println("Actividad Actual - Diseñar Vista Web");
    }

    @Override
    public void diseño_vista_web() {
        System.out.println("Programador Web - Diseña Vista Web");
    }
}
