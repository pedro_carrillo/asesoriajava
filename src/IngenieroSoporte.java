import java.util.List;

public class IngenieroSoporte extends Empleado implements IActividades{
    public List<String> tecnologias_soporte;

    public IngenieroSoporte(){

    }

    public void imprimeDatos(){
        System.out.println("Tecnologias Soporte");
        if(tecnologias_soporte!=null){
            for(String ts : tecnologias_soporte){
                System.out.print(ts + " ");
            }
        }
    }

    @Override
    public void actividad_actual() {
        System.out.println("Actividad Actual - Resolver Tickets");
    }

    @Override
    public void resolver_tickets() {
        System.out.println("Ingeniero de Soporte - Resuelve Ticket...");
    }
}
