import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class ProgramadorMobile extends Programador implements IActividades{
    public List<String> tecnologias_moviles;
    public Integer tiempo_experiencia = ThreadLocalRandom.current().nextInt(1,15);;

    public ProgramadorMobile(){

    }

    public void imprimeDatos(){
        System.out.println("Tecnologias Moviles");
        if(tecnologias_moviles!=null){
            for(String tm : tecnologias_moviles){
                System.out.print(tm + " ");
            }
        }
    }

    @Override
    public void actividad_actual() {
        System.out.println("Actividad Actual - Realizar Mantenimiento a App");
    }

    @Override
    public void mantenimiento_aplicacion() {
        System.out.println("Programador Movil - Da Mantenimiento a App");
    }
}
