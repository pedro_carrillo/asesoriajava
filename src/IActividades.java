public interface IActividades {
    void actividad_actual();
    default void diseño_vista_web(){
        System.out.println("Diseña vista Web...");
    }
    default void mantenimiento_aplicacion(){
        System.out.println("Mantenimiento a App...");
    }
    default void resolver_tickets(){
        System.out.println("Resuelve ticket...");
    }
}
