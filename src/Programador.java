import java.util.List;

public class Programador extends Empleado{
    public List<String> lenguajes;
    public  List<String> frameworks;
    public String titulo_profesional;

    public Programador(){

    }

    public Programador(List<String> lenguajes, List<String> frameworks, String titulo_profesional){
        this.lenguajes = lenguajes;
        this.frameworks = frameworks;
        this.titulo_profesional = titulo_profesional;
    }

    public void imprimeDatos(){
        System.out.println("Lenguajes");
        if(lenguajes!=null){
            for(String l : lenguajes){
                System.out.print(l + " ");
            }
        }
        System.out.println("Frameworks");
        if(frameworks!=null){
            for(String f : frameworks){
                System.out.print(f + " ");
            }
        }
    }

    @Override
    public String toString(){
        String datos = this.getName() + " ID: " + this.getID() + "\n";
        datos += "Lenguajes:\n";
        if(lenguajes!=null){
            for(String l : lenguajes){
                datos += l + " ";
            }
        }
        datos += "\nFrameworks:\n";
        if(frameworks!=null){
            for(String f : frameworks){
                datos += f + " ";
            }
        }
        return datos;
    }
}
