import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Empleado emp = new Empleado(25,"Jose","Avenida Z 123");
        //System.out.println(emp.getName() + " - " + emp.getID());
        //System.out.println("Salario: "+emp.getSalary() + " - Bono: " + emp.getBono());
        List<String> lenguajes = new ArrayList<>();
        lenguajes.add("Java");
        lenguajes.add("C++");
        List<String> frameworks = new ArrayList<>();
        frameworks.add("Spring");
        frameworks.add("Angular");
        Empleado programador = new Programador(lenguajes,frameworks,"ABEC-123");
        programador.setName("Hayate");
        System.out.println(programador.toString());

        //PRUEBAS DE ASIGNACION
        //Programador programadorEmp = new Empleado(); //No
        //ProgramadorWeb programadorWebEmp = new Empleado(); //No
        //ProgramadorMobile programadorMobileEmp = new Empleado(); //No
        //IngenieroSoporte ingenieroSoporteEmp = new Empleado(); //No

        Empleado empleadoProgramador = new Programador(); //Si
        Empleado empleadoProgramadorWeb = new ProgramadorWeb(); //Si
        Empleado empleadoProgramadorMovil = new ProgramadorMobile(); //Si
        Empleado empleadoIngenieroSoporte = new IngenieroSoporte(); //Si

        //ProgramadorWeb programadorWebProgramador = new Programador(); //No
        //ProgramadorMobile programadorMobileProgramador = new Programador(); //No

        Programador programadorProgamadorWeb = new ProgramadorWeb(); //Si
        Programador programadorProgramadorMovil = new ProgramadorMobile(); //Si

        System.out.println("\nUso de los metodos implementados de la interfaz");
        ((ProgramadorMobile) programadorProgramadorMovil).actividad_actual();
        ((ProgramadorMobile) programadorProgramadorMovil).mantenimiento_aplicacion();

        //System.out.println(((ProgramadorWeb) programadorProgamadorWeb).tiempo_experiencia);
    }
}
